#!/usr/bin/env bash

PRIMARY_DOMAIN="site.ccplatform.net"

# Get certificate
/app/acme.sh/acme.sh --home /app/acme.sh --issue \
    -d $PRIMARY_DOMAIN -d site2.ccplatform.net \
    -w /app/web

if [ -f "/app/acme.sh/${PRIMARY_DOMAIN}/${PRIMARY_DOMAIN}.cer" ]; then
    # Update certificate at AWS
    cd /app/acme.sh/${PRIMARY_DOMAIN}
    aws acm import-certificate \
        --region us-east-1 \
        --certificate-arn ${AWS_CERT_ARN} \
        --certificate file://${PRIMARY_DOMAIN}.cer \
        --private-key file://${PRIMARY_DOMAIN}.key \
        --certificate-chain file://fullchain.cer
    # Remove local certificate
    rm -Rf /app/acme.sh/${PRIMARY_DOMAIN}
fi